/*// change this Component to a class!
function CatNav(props){
    // get data from props and use map to build an array of <li>
    // ... code goes here
    return(
        <div>
            {/!* Your Code Here *!/}
        </div>
    )
}*/

class CatNav extends React.Component {
    creatNav() {
        return this.props.data.map((elem, i) => {
            return (
                <li key={i} className="cat-link left valign-wrapper">
                    <i className="material-icons">{elem.icon}</i>{elem.title}
                </li>
            )
        })
    }

    render() {
        return (
            <div>
                <ul className="cat-nav center-align">
                    {this.creatNav()}
                </ul>
            </div>
        )
    }
}
