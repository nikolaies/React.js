import React, { Component } from 'react';

class StateInAction extends Component {
    constructor() {
        super();
        this.state = {
            text: "State In Action",
            text2: "Hello my guy"
        }
        setTimeout(() => {
            this.setState( {
                text: "State Change"
            })
        }, 2000)
    }

    render() {
        return (
            <div>
                <h1>{this.state.text}</h1>
                <h2>{this.state.text2}</h2>
            </div>
        )
    }
}

export default StateInAction