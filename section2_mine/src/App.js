import React from "react";
import './App.css';
import StateInAction from './StateInAction'
import EventAndState from "./EventAndState";
//import StatePractice from "./StatePractice";
import CardSet from "./CardSet";
import cards from "./cards"
import Card from "./Card";



class App extends React.Component{
  render() {
    return (
        <div className="App">
          <StateInAction />
            <h1>---------------------</h1>
            <h2>Events</h2>
            <EventAndState />
            <h1>---------------------</h1>
          <div className="row">
            <CardSet cards={cards} />
          </div>
        </div>
    );
  }
}

export default App;
