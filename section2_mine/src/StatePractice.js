import React, { Component } from "react";

class StatePractice extends Component {
    constructor() {
        super();
        this.state = {
            message: ""
        }
    }

    handleFocus = (event) =>{
        this.setState({
            message: "You agree to Terms by filling out form",

        })
    }

    handleMouse = () => {
        this.setState({
            message: ""
        })
    }

    imageLoad = (event) => {
        if(event.target.width > 100) {
            console.log("Your image is large.")
        }
    }

    render() {
        return (
            <div>
                <input onFocus={this.handleFocus} type='text'/>
                <h3 onMouseEnter={this.handleMouse}>{this.state.message}</h3>
                <img onLoad={this.imageLoad} src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png" />
            </div>
        );
    }
}

export default StatePractice