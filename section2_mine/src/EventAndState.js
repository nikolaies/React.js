import React, {Component} from "react";

class EventAndState extends Component {
    constructor() {
        super();
        this.state = {
            inputText: ""
        }
        // this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({
            inputText: ""
        })
    }

    handleChange = (event) => {
        this.setState({
            inputText: event.target.value
        })
    }

    handleSubmit = (event) => {
        console.log("Form Submitted!")
        this.setState({
            inputText: "State is cool"
        })
        event.preventDefault()
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <h1>{this.state.inputText}</h1>
                    <button onClick={this.handleClick}>Click me</button>
                    <input onChange={this.handleChange} type="text" placeholder="Enter some text!"/>
                </form>
            </div>
        )
    }
}

export default EventAndState