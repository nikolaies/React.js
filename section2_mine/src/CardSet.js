import React, {Component} from "react";
import Card from "./Card";

class CardSet extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.cards)
        this.state = {
            chosenCards: []
        }
    }

    saveCourse = (index) => {
        const copyOfCards = [...this.state.chosenCards];
        copyOfCards.push(this.props.cards[index])
        this.setState({
            chosenCards: copyOfCards
        })
    }

    render() {
        const savedCards = this.state.chosenCards.map((card, i) => {
            return <Card key={i} card={card}/>
        })


        const cardList = this.props.cards.map((card, i) => {
            return (
                <div key={i} className="col s2">
                    <Card card={card} />
                    <button onClick={() => {this.saveCourse(i)}} className="btn waves-light waves-effect">Save</button>
                </div>
            )
        })


        return(
            <div>
                {cardList}
                {savedCards}
            </div>
        )
    }
}

export default CardSet